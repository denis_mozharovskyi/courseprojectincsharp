﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ParserSearchGoogle
{
    class Program
    {
        static void Main(string[] args)
        {
            // Open Chrome browser
            IWebDriver driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            
            // Navigate to URL 
            driver.Url = "https://www.google.com.ua/";
            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(10);

            // Identify the Google search text box
            IWebElement elementInput = driver.FindElement(By.Name("q"));
            
            // Enter the value in the google search text box 
            elementInput.SendKeys("books on C#");

            // Identify the google search button  
            IWebElement elementButton = driver.FindElement(By.Name("btnK"));
            
            // Click on the Google search button from JavaScript
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            executor.ExecuteScript("arguments[0].click();", elementButton);
            
            // Create visible list elements pagination
            List<IWebElement> elementPages = new List<IWebElement>(driver
                    .FindElements(By.CssSelector("tbody > tr > td > a.fl"))
                    .Where(e => e.Displayed));
            
            // Number of pages for parsing (10)
            var pages = elementPages.Count + 1 > 10 ? 10 : elementPages.Count + 1;
            
            // Array all pages with domen names
            JArray array = new JArray();
                     
            for (var i = 1; i <= pages; i++)
            {
                // Array domen names on page
                JArray arrayPages = new JArray();
               
                // Create visible list elements link
                List<IWebElement> elements = new List<IWebElement>(driver
                    .FindElements(By.CssSelector("div.g > div.rc > div.r > a > div.TbwUpd > cite"))
                    .Where(e => e.Displayed));
                
                // Domen names on page write in array
                if (elements.Count > 0)
                {
                    foreach (var item in elements)
                    {
                        string[] strs = item.Text.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        arrayPages.Add(strs[0]);
                    }
                }

                // Object array domen names on page
                JObject jObjectPages = new JObject();
                jObjectPages[$"Page {i}"] = arrayPages;
                array.Add(jObjectPages);

                if (i == pages) break;
                // Link Next page click
                if(pages > 1)
                    driver.FindElement(By.Id("pnnext")).Click();             
            }

            // Object array all pages with domen names
            JObject jObject = new JObject();
            jObject["domens"] = array;

            // Close Chrome browser
            driver.Quit();
            
            using (StreamWriter file = File.CreateText("parseDomenNames.json"))
            {
                JsonSerializer serializer = new JsonSerializer();
                // Serialize object directly into file stream
                serializer.Serialize(file, jObject);
            }

        }
    }
}
